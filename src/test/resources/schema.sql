DROP TABLE IF EXISTS damage;
DROP TABLE IF EXISTS accident;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS location;

CREATE TABLE location(
    location_id IDENTITY NOT NULL PRIMARY KEY,
    street_name VARCHAR(200) NOT NULL,
    house_number VARCHAR(10),
    apartment_number VARCHAR(10)
);

CREATE TABLE customer (
    customer_id IDENTITY NOT NULL PRIMARY KEY,
	first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    driver_license INT NOT NULL UNIQUE,
	location_id INT NOT NULL,
	CONSTRAINT FK_location_customer FOREIGN KEY(location_id) REFERENCES location(location_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE car(
    car_id IDENTITY NOT NULL PRIMARY KEY,
    model VARCHAR(30) NOT NULL,
    release_year INT NOT NULL,
    insurance_policy_id INT NOT NULL,
    car_number VARCHAR(10) NOT NULL,
    customer_id INT NOT NULL,
    UNIQUE(insurance_policy_id),
	UNIQUE(car_number, customer_id),
	CONSTRAINT FK_customer_car FOREIGN KEY(customer_id) REFERENCES customer(customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE accident(
    accident_id IDENTITY NOT NULL PRIMARY KEY,
    report_id INT NOT NULL,
    accident_date DATE NOT NULL,
    car_id INT NOT NULL,
	location_id INT NOT NULL,
	CONSTRAINT FK_location_accident FOREIGN KEY(location_id) REFERENCES location(location_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FK_car_accident FOREIGN KEY(car_id) REFERENCES car(car_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE damage(
    damage_id IDENTITY NOT NULL PRIMARY KEY,
    description TEXT NOT NULL,
    damage_amount NUMERIC NOT NULL,
    accident_id INT NOT NULL,
	CONSTRAINT FK_accident_damage FOREIGN KEY(accident_id) REFERENCES accident(accident_id) ON DELETE CASCADE ON UPDATE CASCADE
);