package com.olpi;

import com.olpi.dao.mapper.*;
import com.olpi.dao.pg_impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class TestConfig {

	@Autowired
	private DataSource dataSource;

    @Bean("carDao")
    @DependsOn("jdbcTemplate")
    public PgCarDao getPgCarDao(){
        return new PgCarDao();
    }

	@Bean("accidentDao")
	@DependsOn("jdbcTemplate")
	public PgAccidentDao getPgAccidentDao(){
		return new PgAccidentDao();
	}

	@Bean("customerDao")
	@DependsOn("jdbcTemplate")
	public PgCustomerDao getPgCustomerDao(){
		return new PgCustomerDao();
	}

	@Bean("damageDao")
	@DependsOn("jdbcTemplate")
	public PgDamageDao getPgDamageDao(){
		return new PgDamageDao();
	}

	@Bean("locationDao")
	@DependsOn("jdbcTemplate")
	public PgLocationDao getPgLocationDao(){
		return new PgLocationDao();
	}

	@Bean("jdbcTemplate")
	@DependsOn("dataSource")
	public JdbcTemplate getJdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}

	@Bean(destroyMethod = "shutdown", name = "dataSource")
    public EmbeddedDatabase getDataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("testData.sql")
                .build();
    }

	@Bean("accidentMapper")
	public AccidentMapper getAccidentMapper(){
    	return new AccidentMapper();
	}

	@Bean("carMapper")
	public CarMapper getCarMapper(){
		return new CarMapper();
	}

	@Bean("customerMapper")
	public CustomerMapper getCustomerMapper(){
		return new CustomerMapper();
	}

	@Bean("damageMapper")
	public DamageMapper getDamageMapper(){
		return new DamageMapper();
	}

	@Bean("locationMapper")
	public LocationMapper getLocationMapper(){
		return new LocationMapper();
	}

}
