package com.olpi.dao.pg_impl;

import com.olpi.TestConfig;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.CarMapper;
import com.olpi.domain.Car;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class PgCarDaoTest {

	private static final String SQL_SELECT_CAR_ID = "SELECT car_id FROM car  WHERE car_id = ?";
	private static final String SQL_SELECT_CAR_BY_ID = "SELECT car_id, model, release_year, insurance_policy_id, car_number, customer_id FROM car  WHERE car_id = ?";
	private static final String SQL_SELECT_LAST_CAR_ID = "SELECT car_id FROM car ORDER BY car_id DESC LIMIT 1";
	private static final String SQL_SELECT_ALL_CARS = "SELECT car_id, model, release_year, insurance_policy_id, car_number, customer_id FROM car;";

	@Autowired
	private PgCarDao carDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void shouldInsertCarAndReturnIdWhenCarDoesNotHaveId() throws DAOException {
		Car actual = Car.builder().model("Audi A6").year(2018).insuranceId(989965874).carNumber("889 PPR 02")
				.customerId(3L).build();
		actual = carDao.createOrUpdate(actual);
		Long actualId = actual.getId();
		Long expectedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CAR_ID, Long.class);
		Assertions.assertEquals(expectedId, actualId);

		Car expected = jdbcTemplate.queryForObject(SQL_SELECT_CAR_BY_ID,
				new Object[]{expectedId}, new CarMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldUpdateCarWhenCallCreateOrUpdateForExistedCar() throws DAOException {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CAR_ID, Long.class);
		int newInsuranceId = 439202763;
		String newCarNumber = "338 UDN 02";
		Long newCustomerId = 3L;
		Car actual = Car.builder().id(existedId).model("Volvo").year(2013).insuranceId(newInsuranceId)
				.carNumber(newCarNumber)
				.customerId(newCustomerId).build();
		actual = carDao.createOrUpdate(actual);
		Car expected = jdbcTemplate.queryForObject(SQL_SELECT_CAR_BY_ID,
				new Object[]{existedId}, new CarMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	final void shouldThrowExceptionWhenCarIdDoesNotExistInBase() {
		Long notExistedId = 99L;
		Car actual = Car.builder().id(notExistedId).model("Audi A6").year(2018).insuranceId(989965874)
				.carNumber("889 PPR 02")
				.customerId(1L).build();
		Assertions.assertThrows(DAOException.class,
				() -> carDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewCarHasExistedInsuranceId() {
		int existedInsuranceId = 96587485;
		Car actual = Car.builder().model("Audi A6").year(2018).insuranceId(existedInsuranceId).carNumber("889 PPR 02")
				.customerId(1L).build();
		Assertions.assertThrows(DAOException.class,
				() -> carDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedCarHasExistedInsuranceId() {
		int existedInsuranceId = 96587485;
		Car actual = Car.builder().id(1L).model("Audi A6").year(2018).insuranceId(existedInsuranceId)
				.carNumber("889 PPR 02")
				.customerId(1L).build();
		Assertions.assertThrows(DAOException.class,
				() -> carDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewCarHasInvalidCustomerId() {
		Long invalidCustomerId = 99L;
		Car actual = Car.builder().model("Audi A6").year(2018).insuranceId(989965874).carNumber("889 PPR 02")
				.customerId(invalidCustomerId).build();
		Assertions.assertThrows(DAOException.class,
				() -> carDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedCarHasInvalidCustomerId() {
		Long invalidCustomerId = 99L;
		Car actual = Car.builder().id(1L).model("Audi A6").year(2018).insuranceId(989965874).carNumber("889 PPR 02")
				.customerId(invalidCustomerId).build();
		Assertions.assertThrows(DAOException.class,
				() -> carDao.createOrUpdate(actual));
	}

	@Test
	public void shouldReturnOptionalCarWhenIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CAR_ID, Long.class);
		Optional<Car> actual = carDao.findById(existedId);
		Optional<Car> expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_CAR_BY_ID,
				new Object[]{existedId}, new CarMapper()));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnOptionalEmptyObjectWhenIdDoesNotExist() {
		Optional<Car> actual = carDao.findById(100L);
		Optional<Car> expected;
		try {
			expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_CAR_BY_ID,
					new Object[]{100L}, new CarMapper()));
		} catch (EmptyResultDataAccessException e) {
			expected = Optional.empty();
		}
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnListOfCarWhenCallFindAll() {
		List<Car> actual = carDao.findAll();
		List<Car> expected = jdbcTemplate.query(SQL_SELECT_ALL_CARS, new CarMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldRemoveCarWhenCarIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CAR_ID, Long.class);
		carDao.remove(existedId);
		Assertions.assertThrows(EmptyResultDataAccessException.class,
				() -> jdbcTemplate.queryForObject(SQL_SELECT_CAR_ID,
						new Object[]{existedId}, Long.class));
	}

}