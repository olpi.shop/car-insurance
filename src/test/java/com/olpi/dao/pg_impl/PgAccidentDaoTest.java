package com.olpi.dao.pg_impl;

import com.olpi.TestConfig;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.AccidentMapper;
import com.olpi.domain.Accident;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class PgAccidentDaoTest {
	private static final String SQL_SELECT_ACCIDENT_ID = "SELECT accident_id FROM accident  WHERE accident_id = ?";
	private static final String SQL_SELECT_ACCIDENT_BY_ID = "SELECT accident_id, report_id, accident_date, car_id, location_id FROM accident  WHERE accident_id = ?";
	private static final String SQL_SELECT_LAST_ACCIDENT_ID = "SELECT accident_id FROM accident ORDER BY accident_id DESC LIMIT 1";
	private static final String SQL_SELECT_ALL_ACCIDENTS = "SELECT  accident_id, report_id, accident_date, car_id, location_id  FROM accident;";
	@Autowired
	private PgAccidentDao accidentDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void shouldInsertAccidentAndReturnIdWhenAccidentDoesNotHaveId() throws DAOException {
		Accident actual = Accident.builder().reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21)).carId(1L)
				.locationId(1L)
				.build();
		actual = accidentDao.createOrUpdate(actual);
		Long actualId = actual.getId();
		Long expectedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_ACCIDENT_ID, Long.class);
		Assertions.assertEquals(expectedId, actualId);

		Accident expected = jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_BY_ID,
				new Object[]{expectedId}, new AccidentMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldUpdateAccidentWhenCallCreateOrUpdateForExistedCar() throws DAOException {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_ACCIDENT_ID, Long.class);
		int newReportId = 85632145;
		LocalDate newDate = LocalDate.of(2020, 3, 22);
		Long newCarId = 3L;
		Long newLocationId = 4L;
		Accident actual = Accident.builder().id(existedId).reportId(newReportId).accidentDate(newDate).carId(newCarId)
				.locationId(newLocationId)
				.build();
		accidentDao.createOrUpdate(actual);

		Accident expected = jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_BY_ID,
				new Object[]{existedId}, new AccidentMapper());

		Assertions.assertEquals(expected, actual);
	}

	@Test
	final void shouldThrowExceptionWhenAccidentIdDoesNotExistInBase() {
		Long notExistedId = 99L;
		Accident actual = Accident.builder().id(notExistedId).reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21))
				.carId(3L)
				.locationId(2L)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> accidentDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewAccidentHasInvalidCarId() {
		Long invalidCarId = 99L;
		Accident actual = Accident.builder().reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21))
				.carId(invalidCarId)
				.locationId(2L)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> accidentDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewAccidentHasInvalidLocationId() {
		Long invalidLocationId = 99L;
		Accident actual = Accident.builder().reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21))
				.carId(3L)
				.locationId(invalidLocationId)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> accidentDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedAccidentHasInvalidCarId() {
		Long invalidCarId = 99L;
		Accident actual = Accident.builder().id(3L).reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21))
				.carId(invalidCarId)
				.locationId(2L)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> accidentDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedAccidentHasInvalidLocationId() {
		Long invalidLocationId = 99L;
		Accident actual = Accident.builder().id(3L).reportId(99000389).accidentDate(LocalDate.of(2018, 2, 21))
				.carId(3L)
				.locationId(invalidLocationId)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> accidentDao.createOrUpdate(actual));
	}

	@Test
	public void shouldReturnOptionalAccidentWhenIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_ACCIDENT_ID, Long.class);
		Optional<Accident> actual = accidentDao.findById(existedId);
		Optional<Accident> expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_BY_ID,
				new Object[]{existedId}, new AccidentMapper()));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnOptionalEmptyObjectWhenIdDoesNotExist() {
		Optional<Accident> actual = accidentDao.findById(100L);
		Optional<Accident> expected;
		try {
			expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_BY_ID,
					new Object[]{100L}, new AccidentMapper()));
		} catch (EmptyResultDataAccessException e) {
			expected = Optional.empty();
		}
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnListOfAccidentsWhenCallFindAll() {
		List<Accident> actual = accidentDao.findAll();
		List<Accident> expected = jdbcTemplate.query(SQL_SELECT_ALL_ACCIDENTS, new AccidentMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldRemoveAccidentWhenAccidentIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_ACCIDENT_ID, Long.class);
		accidentDao.remove(existedId);
		Assertions.assertThrows(EmptyResultDataAccessException.class,
				() -> jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_ID,
						new Object[]{existedId}, Long.class));
	}

}