package com.olpi.dao.pg_impl;

import com.olpi.TestConfig;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.DamageMapper;
import com.olpi.domain.Damage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class PgDamageDaoTest {
	private static final String SQL_SELECT_DAMAGE_ID = "SELECT damage_id FROM damage  WHERE damage_id = ?";
	private static final String SQL_SELECT_DAMAGE_BY_ID = "SELECT damage_id, description, damage_amount, accident_id FROM damage  WHERE damage_id = ?";
	private static final String SQL_SELECT_LAST_DAMAGE_ID = "SELECT damage_id FROM damage ORDER BY damage_id DESC LIMIT 1";
	private static final String SQL_SELECT_ALL_DAMAGES = "SELECT damage_id, description, damage_amount, accident_id FROM damage;";

	@Autowired
	private PgDamageDao damageDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void shouldInsertDamageAndReturnIdWhenDamageDoesNotHaveId() throws DAOException {
		Damage actual = Damage.builder().description("Windshield damage").damageAmount(BigDecimal.valueOf(233.45))
				.accidentId(6L).build();
		actual = damageDao.createOrUpdate(actual);
		Long actualId = actual.getId();
		Long expectedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_DAMAGE_ID, Long.class);
		Assertions.assertEquals(expectedId, actualId);

		Damage expected = jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_BY_ID,
				new Object[]{expectedId}, new DamageMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldUpdateDamageWhenCallCreateOrUpdateForExistedDamage() throws DAOException {
		Long existedDamageId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_DAMAGE_ID, Long.class);
		BigDecimal newDamageAmount = BigDecimal.valueOf(233.45);
		Long newAccidentId = 5L;
		Damage actual = Damage.builder().id(existedDamageId).description("Windshield damage")
				.damageAmount(newDamageAmount)
				.accidentId(newAccidentId).build();
		actual = damageDao.createOrUpdate(actual);
		Damage expected = jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_BY_ID,
				new Object[]{existedDamageId}, new DamageMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	final void shouldThrowExceptionWhenDamageIdDoesNotExistInBase() {
		Long notExistedId = 99L;
		Damage actual = Damage.builder().id(notExistedId).description("Windshield damage")
				.damageAmount(BigDecimal.valueOf(233.45))
				.accidentId(1L).build();
		Assertions.assertThrows(DAOException.class,
				() -> damageDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewDamageHasInvalidAccidentId() {
		Long invalidAccidentId = 99L;
		Damage actual = Damage.builder().description("Windshield damage").damageAmount(BigDecimal.valueOf(233.45))
				.accidentId(invalidAccidentId).build();
		Assertions.assertThrows(DAOException.class,
				() -> damageDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedDamageHasInvalidAccidentId() {
		Long invalidAccidentId = 99L;
		Damage actual = Damage.builder().id(1L).description("Windshield damage")
				.damageAmount(BigDecimal.valueOf(233.45))
				.accidentId(invalidAccidentId).build();
		Assertions.assertThrows(DAOException.class,
				() -> damageDao.createOrUpdate(actual));
	}

	@Test
	public void shouldReturnOptionalDamageWhenIdExists() {
		Long existedDamageId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_DAMAGE_ID, Long.class);
		Optional<Damage> actual = damageDao.findById(existedDamageId);
		Optional<Damage> expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_BY_ID,
				new Object[]{existedDamageId}, new DamageMapper()));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnOptionalEmptyObjectWhenIdDoesNotExist() {
		Long notExistedCustomerId = 99L;
		Optional<Damage> actual = damageDao.findById(notExistedCustomerId);
		Optional<Damage> expected;
		try {
			expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_BY_ID,
					new Object[]{notExistedCustomerId}, new DamageMapper()));
		} catch (EmptyResultDataAccessException e) {
			expected = Optional.empty();
		}
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnListOfDamageWhenCallFindAll() {
		List<Damage> actual = damageDao.findAll();
		List<Damage> expected = jdbcTemplate.query(SQL_SELECT_ALL_DAMAGES, new DamageMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldRemoveDamageWhenDamageIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_DAMAGE_ID, Long.class);
		damageDao.remove(existedId);
		Assertions.assertThrows(EmptyResultDataAccessException.class,
				() -> jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_ID,
						new Object[]{existedId}, Long.class));
	}

}