package com.olpi.dao.pg_impl;

import com.olpi.TestConfig;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.CustomerMapper;
import com.olpi.domain.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class PgCustomerDaoTest {

	private static final String SQL_SELECT_CUSTOMER_ID = "SELECT customer_id FROM customer  WHERE customer_id = ?";
	private static final String SQL_SELECT_LAST_CUSTOMER_ID = "SELECT customer_id FROM customer ORDER BY customer_id DESC LIMIT 1";
	private static final String SQL_SELECT_CUSTOMER_BY_ID = "SELECT customer_id, first_name, last_name, driver_license, location_id  FROM customer  WHERE customer_id = ?";
	private static final String SQL_SELECT_ALL_CUSTOMERS = "SELECT customer_id, first_name, last_name, driver_license, location_id FROM customer;";

	@Autowired
	private PgCustomerDao customerDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void shouldInsertCustomerAndReturnIdWhenCustomerDoesNotHaveId() throws DAOException {
		Customer actual = Customer.builder().firstName("Lil").lastName("Jon").driverLicense(259685).locationId(2L)
				.build();
		actual = customerDao.createOrUpdate(actual);
		Long actualId = actual.getId();
		Long expectedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CUSTOMER_ID, Long.class);
		Assertions.assertEquals(expectedId, actualId);

		Customer expected = jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_BY_ID,
				new Object[]{expectedId}, new CustomerMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldUpdateCustomerWhenCallCreateOrUpdateForExistedCustomer() throws DAOException {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CUSTOMER_ID, Long.class);
		int newDriverLicense = 965589;
		Long newLocationId = 2L;
		Customer actual = Customer.builder().id(existedId).firstName("Ева").lastName("Петухова")
				.driverLicense(newDriverLicense).locationId(newLocationId).build();
		actual = customerDao.createOrUpdate(actual);

		Customer expected = jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_BY_ID,
				new Object[]{existedId}, new CustomerMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	final void shouldThrowExceptionWhenCustomerIdDoesNotExistInBase() {
		Long notExistedId = 99L;
		Customer actual = Customer.builder().id(notExistedId).firstName("Lil").lastName("Jon").driverLicense(259685)
				.locationId(2L).build();
		Assertions.assertThrows(DAOException.class,
				() -> customerDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewCustomerHasExistedDriverLicense() {
		int existedDriverLicense = 122234312;
		Customer actual = Customer.builder().firstName("Lil").lastName("Jon").driverLicense(existedDriverLicense)
				.locationId(2L)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> customerDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedCustomerHasExistedDriverLicense() {
		int existedDriverLicense = 708455375;
		Customer actual = Customer.builder().id(1L).firstName("Lil").lastName("Jon").driverLicense(existedDriverLicense)
				.locationId(2L)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> customerDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenNewCustomerHasInvalidLocationId() {
		Long invalidLocationId = 99L;
		Customer actual = Customer.builder().firstName("Lil").lastName("Jon").driverLicense(259685)
				.locationId(invalidLocationId)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> customerDao.createOrUpdate(actual));
	}

	@Test
	final void shouldThrowExceptionWhenUpdatedCustomerHasInvalidLocationId() {
		Long invalidLocationId = 99L;
		Customer actual = Customer.builder().id(1L).firstName("Lil").lastName("Jon").driverLicense(259685)
				.locationId(invalidLocationId)
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> customerDao.createOrUpdate(actual));
	}

	@Test
	public void shouldReturnOptionalCustomerWhenIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CUSTOMER_ID, Long.class);
		Optional<Customer> actual = customerDao.findById(existedId);
		Optional<Customer> expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_BY_ID,
				new Object[]{existedId}, new CustomerMapper()));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnOptionalEmptyObjectWhenIdDoesNotExist() {
		Long notExistedCustomerId = 99L;
		Optional<Customer> actual = customerDao.findById(notExistedCustomerId);
		Optional<Customer> expected;
		try {
			expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_BY_ID,
					new Object[]{notExistedCustomerId}, new CustomerMapper()));
		} catch (EmptyResultDataAccessException e) {
			expected = Optional.empty();
		}
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnListOfCustomerWhenCallFindAll() {
		List<Customer> actual = customerDao.findAll();
		List<Customer> expected = jdbcTemplate.query(SQL_SELECT_ALL_CUSTOMERS, new CustomerMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldRemoveCustomerWhenCustomerIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_CUSTOMER_ID, Long.class);
		customerDao.remove(existedId);
		Assertions.assertThrows(EmptyResultDataAccessException.class,
				() -> jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_ID,
						new Object[]{existedId}, Long.class));
	}

}