package com.olpi.dao.pg_impl;

import com.olpi.TestConfig;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.LocationMapper;
import com.olpi.domain.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class PgLocationDaoTest {

	private static final String SQL_SELECT_LOCATION_ID = "SELECT location_id FROM location  WHERE location_id = ?";
	private static final String SQL_SELECT_LOCATION_BY_ID = "SELECT location_id, street_name, house_number, apartment_number FROM location  WHERE location_id = ?";
	private static final String SQL_SELECT_LAST_LOCATION_ID = "SELECT location_id FROM location ORDER BY location_id DESC LIMIT 1";
	private static final String SQL_SELECT_ALL_LOCATIONS = "SELECT location_id, street_name, house_number, apartment_number FROM location;";

	@Autowired
	private PgLocationDao locationDao;
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Test
	public void shouldInsertLocationAndReturnIdWhenLocationDoesNotHaveId() throws DAOException {
		Location actual = Location.builder().streetName("Пушкина").house("32").apartment("223").build();
		actual = locationDao.createOrUpdate(actual);
		Long actualId = actual.getId();
		Long expectedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_LOCATION_ID, Long.class);
		Assertions.assertEquals(expectedId, actualId);

		Location expected = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
				new Object[]{expectedId}, new LocationMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldUpdateLocationWhenCallCreateOrUpdateForExistedLocation() throws DAOException {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_LOCATION_ID, Long.class);
		String newStreet = "Пушкина";
		Location actual = Location.builder().id(existedId).streetName(newStreet).build();
		actual = locationDao.createOrUpdate(actual);
		Location expected = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
				new Object[]{existedId}, new LocationMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	final void shouldThrowExceptionWhenLocationIdDoesNotExistInBase() {
		Long notExistedId = 99L;
		Location actual = Location.builder().id(notExistedId).streetName("Пушкина").house("32").apartment("223")
				.build();
		Assertions.assertThrows(DAOException.class,
				() -> locationDao.createOrUpdate(actual));
	}

	@Test
	public void shouldReturnOptionalLocationWhenIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_LOCATION_ID, Long.class);
		Optional<Location> actual = locationDao.findById(existedId);
		Optional<Location> expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
				new Object[]{existedId}, new LocationMapper()));
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnOptionalEmptyObjectWhenIdDoesNotExist() {
		Long notExistedCustomerId = 99L;
		Optional<Location> actual = locationDao.findById(notExistedCustomerId);
		Optional<Location> expected;
		try {
			expected = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
					new Object[]{notExistedCustomerId}, new LocationMapper()));
		} catch (EmptyResultDataAccessException e) {
			expected = Optional.empty();
		}
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnListOfLocationWhenCallFindAll() {
		List<Location> actual = locationDao.findAll();
		List<Location> expected = jdbcTemplate.query(SQL_SELECT_ALL_LOCATIONS, new LocationMapper());
		Assertions.assertEquals(expected, actual);
	}

	@Test
	public void shouldRemoveLocationWhenLocationIdExists() {
		Long existedId = jdbcTemplate.queryForObject(SQL_SELECT_LAST_LOCATION_ID, Long.class);
		locationDao.remove(existedId);
		Assertions.assertThrows(EmptyResultDataAccessException.class,
				() -> jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_ID,
						new Object[]{existedId}, Long.class));
	}
}