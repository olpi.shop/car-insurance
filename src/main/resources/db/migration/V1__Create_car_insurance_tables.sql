DROP TABLE IF EXISTS damage;
DROP TABLE IF EXISTS accident;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS location;

CREATE TABLE location(
    location_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    street_name VARCHAR(200) NOT NULL,
    house_number VARCHAR(10),
    apartment_number VARCHAR(10),
	CONSTRAINT PK_location_location_id PRIMARY KEY(location_id)
);
ALTER TABLE location OWNER TO admin;

CREATE TABLE customer (
    customer_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    driver_license INT NOT NULL UNIQUE,
	location_id INT NOT NULL,
	CONSTRAINT FK_location_customer FOREIGN KEY(location_id) REFERENCES location(location_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT PK_customer_customer_id PRIMARY KEY(customer_id)
);
ALTER TABLE customer OWNER TO admin;

CREATE TABLE car(
    car_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    model VARCHAR(30) NOT NULL,
    release_year INT NOT NULL,
    insurance_policy_id INT NOT NULL UNIQUE,
    car_number VARCHAR(10) NOT NULL,
    customer_id INT NOT NULL,
	UNIQUE(car_number, customer_id),
	CONSTRAINT FK_customer_car FOREIGN KEY(customer_id) REFERENCES customer(customer_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT PK_car_car_id PRIMARY KEY(car_id)
);
ALTER TABLE car OWNER TO admin;

CREATE TABLE accident(
    accident_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    report_id INT NOT NULL,
    accident_date DATE NOT NULL,
    car_id INT NOT NULL,
	location_id INT NOT NULL,
	CONSTRAINT FK_location_accident FOREIGN KEY(location_id) REFERENCES location(location_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT FK_car_accident FOREIGN KEY(car_id) REFERENCES car(car_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT PK_accident_accident_id PRIMARY KEY(accident_id)
);
ALTER TABLE accident OWNER TO admin;

CREATE TABLE damage(
    damage_id INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    description TEXT NOT NULL,
    damage_amount NUMERIC NOT NULL,
    accident_id INT NOT NULL,
	CONSTRAINT FK_accident_damage FOREIGN KEY(accident_id) REFERENCES accident(accident_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT PK_damage_damage_id PRIMARY KEY(damage_id)
);
ALTER TABLE damage OWNER TO admin;
