package com.olpi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Builder
@Data
public class Location extends Entity{
    private static final long serialVersionUID = -9011175813599621020L;

    private Long id;
    private String streetName;
    private String house;
    private String apartment;

}
