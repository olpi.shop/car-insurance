package com.olpi.domain;

import java.io.Serializable;

public abstract class Entity implements Serializable, Cloneable {

    private static final long serialVersionUID = -2390916097400431407L;

    public abstract boolean equals(Object obj);

    public abstract int hashCode();

}