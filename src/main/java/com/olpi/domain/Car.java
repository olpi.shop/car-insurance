package com.olpi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Builder
@Data
public class Car extends Entity{
    private static final long serialVersionUID = -267272676293412740L;

    private Long id;
    private String model;
    private int year;
    private int insuranceId;
    private String carNumber;
    private Long customerId;
}
