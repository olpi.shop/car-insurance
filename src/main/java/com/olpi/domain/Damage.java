package com.olpi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = false)
@Builder
@Data
public class Damage extends Entity{
    private static final long serialVersionUID = 5733922946350444115L;

    private Long id;
    private String description;
    private BigDecimal damageAmount;
    private Long accidentId;

}
