package com.olpi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = false)
@Builder
@Data
public class Accident extends Entity{
    private static final long serialVersionUID = 7688883168319255076L;
    private Long id;
    private int reportId;
    private LocalDate accidentDate;
    private Long carId;
    private Long locationId;

}
