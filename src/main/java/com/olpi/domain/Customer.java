package com.olpi.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Builder
@Data
public class Customer extends Entity{
    private static final long serialVersionUID = -7085816590492561006L;

    private Long id;
    private String firstName;
    private String lastName;
    private int driverLicense;
    private Long locationId;

}
