package com.olpi;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = {"com.olpi"})
public class AppConfig {
    private static final HikariConfig config = new HikariConfig( "src/main/resources/dataSource.properties" );
    private static final HikariDataSource dataSource = new HikariDataSource(config);

    @Bean(destroyMethod = "close")
    public DataSource getDataSource() {
        return  dataSource;
    }

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(getDataSource());
    }


}
