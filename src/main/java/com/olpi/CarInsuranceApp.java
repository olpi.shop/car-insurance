package com.olpi;

import com.olpi.dao.CarDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.pg_impl.PgCarDao;
import com.olpi.domain.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@Slf4j
public class CarInsuranceApp {

	public static void main(String[] args) throws DAOException {
		ApplicationContext context =
				new AnnotationConfigApplicationContext(AppConfig.class);

		CarDao carDao = context.getBean("pgCarDao", PgCarDao.class);
		Car car = Car.builder().id(15L).model("Toyota Mark 2").year(2011).insuranceId(25996).carNumber("Z001ZZZ")
				.customerId(4L).build();
		carDao.createOrUpdate(car);
	}
}
