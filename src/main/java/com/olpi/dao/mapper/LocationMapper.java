package com.olpi.dao.mapper;

import com.olpi.domain.Location;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class LocationMapper implements RowMapper<Location> {
	public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
		Location location = Location.builder().streetName(rs.getString("street_name"))
				.house(rs.getString("house_number")).apartment(rs.getString("apartment_number")).build();
		location.setId(rs.getLong("location_id"));
		return location;
	}
}
