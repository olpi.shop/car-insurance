package com.olpi.dao.mapper;

import com.olpi.domain.Car;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CarMapper implements RowMapper<Car> {
    public Car mapRow(ResultSet rs, int rowNum) throws SQLException {
        Car car = Car.builder().model(rs.getString("model")).year(rs.getInt("release_year"))
                .insuranceId(rs.getInt("insurance_policy_id")).carNumber(rs.getString("car_number"))
                .customerId(rs.getLong("customer_id")).build();
        car.setId(rs.getLong("car_id"));
        return car;
    }
}
