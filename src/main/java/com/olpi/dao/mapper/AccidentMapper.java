package com.olpi.dao.mapper;


import com.olpi.domain.Accident;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AccidentMapper implements RowMapper<Accident> {
	public Accident mapRow(ResultSet rs, int rowNum) throws SQLException {
		Accident accident = Accident.builder().reportId(rs.getInt("report_id")).accidentDate(rs.getDate("accident_date")
				.toLocalDate()).carId(rs.getLong("car_id")).locationId(rs.getLong("location_id")).build();
		accident.setId(rs.getLong("accident_id"));
		return accident;
	}
}
