package com.olpi.dao.mapper;


import com.olpi.domain.Customer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CustomerMapper implements RowMapper<Customer> {
	public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
		Customer customer = Customer.builder().firstName(rs.getString("first_name")).lastName(rs.getString("last_name"))
				.driverLicense(rs.getInt("driver_license")).locationId(rs.getLong("location_id")).build();
		customer.setId(rs.getLong("customer_id"));
		return customer;
	}
}
