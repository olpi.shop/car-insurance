package com.olpi.dao.mapper;

import com.olpi.domain.Damage;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DamageMapper implements RowMapper<Damage> {
	public Damage mapRow(ResultSet rs, int rowNum) throws SQLException {
		Damage damage = Damage.builder().description(rs.getString("description"))
				.damageAmount(rs.getBigDecimal("damage_amount")).accidentId(rs.getLong("accident_id")).build();
		damage.setId(rs.getLong("damage_id"));
		return damage;
	}
}
