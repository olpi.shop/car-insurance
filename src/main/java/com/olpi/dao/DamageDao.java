package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Damage;

import java.util.List;
import java.util.Optional;

public interface DamageDao extends EntityDao<Long, Damage> {

	Damage createOrUpdate(Damage damage) throws DAOException;

	Optional<Damage> findById(Long id);

	List<Damage> findAll();

	void remove(Long id);
}
