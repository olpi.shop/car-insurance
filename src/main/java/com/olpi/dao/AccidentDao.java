package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Accident;

import java.util.List;
import java.util.Optional;

public interface AccidentDao extends EntityDao<Long, Accident> {

	Accident createOrUpdate(Accident accident) throws DAOException;

	Optional<Accident> findById(Long id);

	List<Accident> findAll();

	void remove(Long id);
}
