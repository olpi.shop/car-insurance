package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Location;

import java.util.List;
import java.util.Optional;

public interface LocationDao extends EntityDao<Long, Location>{

    Location createOrUpdate(Location location) throws DAOException;

    Optional<Location> findById(Long id);

    List<Location> findAll();

    void remove(Long id);
}
