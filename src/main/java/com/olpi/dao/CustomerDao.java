package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerDao extends EntityDao<Long, Customer> {

    Customer createOrUpdate(Customer customer) throws DAOException;

    Optional<Customer> findById(Long id);

    List<Customer> findAll();

    void remove(Long id);

}
