package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Accident;
import com.olpi.domain.Entity;

import java.util.List;
import java.util.Optional;

public interface EntityDao <K, T extends Entity> {

	T createOrUpdate(T t) throws DAOException;

	Optional<T> findById(K id);

	List<T> findAll();

	void remove(K id);
}
