package com.olpi.dao.pg_impl;

import com.olpi.dao.CarDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.CarMapper;
import com.olpi.domain.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Repository
public class PgCarDao implements CarDao {

	private static final String SQL_CAR_TABLE = "car";
	private static final String SQL_CAR_ID = "car_id";
	private static final String SQL_UPDATE_CAR = "UPDATE  car SET model = ?, release_year = ?, insurance_policy_id = ?, car_number = ?, customer_id = ? WHERE car_id = ?;";
	private static final String SQL_DELETE_CAR = "DELETE FROM car WHERE car_id = ?;";
	private static final String SQL_SELECT_CAR_BY_ID = "SELECT car_id, model, release_year, insurance_policy_id, car_number, customer_id FROM car  WHERE car_id = ?";
	private static final String SQL_SELECT_ALL_CARS = "SELECT car_id, model, release_year, insurance_policy_id, car_number, customer_id FROM car;";
	private static final String PARAMETER_MODEL = "model";
	private static final String PARAMETER_RELEASE_YEAR = "release_year";
	private static final String PARAMETER_INSURANCE_ID = "insurance_policy_id";
	private static final String PARAMETER_CAR_NUMBER = "car_number";
	private static final String PARAMETER_CUSTOMER_ID = "customer_id";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	@Autowired
	private CarMapper carMapper;

	@PostConstruct
	private void initSimpleJdbcInsert() {
		simpleJdbcInsert = new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
				.withTableName(SQL_CAR_TABLE).usingGeneratedKeyColumns(SQL_CAR_ID);
		log.debug("SimpleJdbcInsert was created in PgCarDao");
	}

	@Override
	public Car createOrUpdate(Car car) throws DAOException {
		if (car.getId() == null) {
			return create(car);
		} else if (findById(car.getId()).isPresent()) {
			return update(car);
		} else {
			throw new DAOException(
					"This car has an ID but does not exist in the database!");
		}
	}

	@Override
	public Optional<Car> findById(Long id) {
		try {
			Car car = jdbcTemplate.queryForObject(SQL_SELECT_CAR_BY_ID,
					new Object[]{id}, carMapper);
			log.debug("Car with ID: {} was found", id);
			return Optional.ofNullable(car);
		} catch (EmptyResultDataAccessException e) {
			log.debug("There is not car with ID: {}", id);
			return Optional.empty();
		}
	}

	@Override
	public List<Car> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_CARS, carMapper);
	}

	@Override
	public void remove(Long id) {
		jdbcTemplate.update(SQL_DELETE_CAR, id);
		log.debug("Car with Id: {} was removed", id);
	}

	private Car create(Car car) throws DAOException {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PARAMETER_MODEL, car.getModel());
		parameters.put(PARAMETER_RELEASE_YEAR, car.getYear());
		parameters.put(PARAMETER_INSURANCE_ID, car.getInsuranceId());
		parameters.put(PARAMETER_CAR_NUMBER, car.getCarNumber());
		parameters.put(PARAMETER_CUSTOMER_ID, car.getCustomerId());
		try {
			Long createdId = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
			car.setId(createdId);
			log.debug("create car with id {}", createdId);
		} catch (DuplicateKeyException ex) {
			throw new DAOException(
					"This car has an insurance Id or combine (car number and customer id) which has already existed in the database!");
		} catch (DataIntegrityViolationException e) {
			throw new DAOException(
					"This car has an customer ID which does not exist in the database!");
		}
		return car;
	}

	private Car update(Car car) throws DAOException {
		try {
			jdbcTemplate.update(SQL_UPDATE_CAR, car.getModel(), car.getYear(),
					car.getInsuranceId(), car.getCarNumber(), car.getCustomerId(), car.getId());
		} catch (DuplicateKeyException ex) {
			throw new DAOException(
					"This car has an insurance Id or combine (car number and customer id) which has already existed in the database!");
		} catch (DataIntegrityViolationException e) {
			throw new DAOException(
					"This car has an customer ID which does not exist in the database!");
		}
		log.debug("update car with id {}", car.getId());
		return car;
	}
}
