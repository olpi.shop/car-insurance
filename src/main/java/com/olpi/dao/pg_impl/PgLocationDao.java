package com.olpi.dao.pg_impl;

import com.olpi.dao.LocationDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.LocationMapper;
import com.olpi.domain.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Repository
public class PgLocationDao implements LocationDao {

	private static final String SQL_LOCATION_TABLE = "location";
	private static final String SQL_LOCATION_ID = "location_id";
	private static final String SQL_UPDATE_LOCATION = "UPDATE  location SET street_name = ?, house_number = ?, apartment_number = ? WHERE location_id = ?;";
	private static final String SQL_DELETE_LOCATION = "DELETE FROM location WHERE location_id = ?;";
	private static final String SQL_SELECT_LOCATION_BY_ID = "SELECT location_id, street_name, house_number, apartment_number FROM location  WHERE location_id = ?";
	private static final String SQL_SELECT_ALL_LOCATIONS = "SELECT location_id, street_name, house_number, apartment_number FROM location;";

	private static final String PARAMETER_STREET_NAME = "street_name";
	private static final String PARAMETER_HOUSE_NUMBER = "house_number";
	private static final String PARAMETER_APARTMENT_NUMBER = "apartment_number";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	@Autowired
	private LocationMapper locationMapper;

	@PostConstruct
	private void init() {
		simpleJdbcInsert = new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
				.withTableName(SQL_LOCATION_TABLE).usingGeneratedKeyColumns(SQL_LOCATION_ID);
		log.debug("SimpleJdbcInsert was created in PgLocationDao");
	}

	@Override
	public Location createOrUpdate(Location location) throws DAOException {
		if (location.getId() == null) {
			return create(location);
		} else if (findById(location.getId()).isPresent()) {
			return update(location);
		} else {
			throw new DAOException(
					"This location has an ID but does not exist in the database!");
		}
	}

	@Override
	public Optional<Location> findById(Long id) {
		try {
			Location location = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
					new Object[]{id}, locationMapper);
			log.debug("Location with ID: {} was found", id);
			return Optional.ofNullable(location);
		} catch (EmptyResultDataAccessException e) {
			log.debug("There is not location with ID: {}", id);
			return Optional.empty();
		}
	}

	@Override
	public List<Location> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_LOCATIONS, locationMapper);
	}

	@Override
	public void remove(Long id) {
		jdbcTemplate.update(SQL_DELETE_LOCATION, id);
		log.debug("Location with Id: {} was removed", id);
	}

	private Location create(Location location) {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PARAMETER_STREET_NAME, location.getStreetName());
		parameters.put(PARAMETER_HOUSE_NUMBER, location.getHouse());
		parameters.put(PARAMETER_APARTMENT_NUMBER, location.getApartment());
		Long createdId = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
		location.setId(createdId);
		log.debug("create location with id {}", createdId);
		return location;
	}

	private Location update(Location location) {
		jdbcTemplate.update(SQL_UPDATE_LOCATION, location.getStreetName(), location.getHouse(), location
				.getApartment(), location.getId());
		log.debug("update location with id {}", location.getId());
		return location;
	}
}
