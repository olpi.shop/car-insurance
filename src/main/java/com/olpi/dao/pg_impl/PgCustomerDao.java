package com.olpi.dao.pg_impl;

import com.olpi.dao.CustomerDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.CustomerMapper;
import com.olpi.domain.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Repository
public class PgCustomerDao implements CustomerDao {

	private static final String SQL_CUSTOMER_TABLE = "customer";
	private static final String SQL_CUSTOMER_ID = "customer_id";
	private static final String SQL_UPDATE_CUSTOMER = "UPDATE  customer SET first_name = ?, last_name = ?, driver_license = ?, location_id = ? WHERE customer_id = ?;";
	private static final String SQL_DELETE_CUSTOMER = "DELETE FROM customer WHERE customer_id = ?;";
	private static final String SQL_SELECT_CUSTOMER_BY_ID = "SELECT customer_id, first_name, last_name, driver_license, location_id  FROM customer  WHERE customer_id = ?";
	private static final String SQL_SELECT_ALL_CUSTOMERS = "SELECT customer_id, first_name, last_name, driver_license, location_id FROM customer;";
	private static final String PARAMETER_FIRST_NAME = "first_name";
	private static final String PARAMETER_LAST_NAME = "last_name";
	private static final String PARAMETER_DRIVER_LICENSE = "driver_license";
	private static final String PARAMETER_LOCATION_ID = "location_id";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	@Autowired
	private CustomerMapper customerMapper;

	@PostConstruct
	private void init() {
		simpleJdbcInsert = new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
				.withTableName(SQL_CUSTOMER_TABLE).usingGeneratedKeyColumns(SQL_CUSTOMER_ID);
		log.debug("SimpleJdbcInsert was created in PgCustomerDao");
	}

	@Override
	public Customer createOrUpdate(Customer customer) throws DAOException {
		if (customer.getId() == null) {
			return create(customer);
		} else if (findById(customer.getId()).isPresent()) {
			return update(customer);
		} else {
			throw new DAOException(
					"This customer has an ID but does not exist in the database!");
		}
	}

	@Override
	public Optional<Customer> findById(Long id) {
		try {
			Customer customer = jdbcTemplate.queryForObject(SQL_SELECT_CUSTOMER_BY_ID,
					new Object[]{id}, customerMapper);
			log.debug("Customer with ID: {} was found", id);
			return Optional.ofNullable(customer);
		} catch (EmptyResultDataAccessException e) {
			log.debug("There is not customer with ID: {}", id);
			return Optional.empty();
		}
	}

	@Override
	public List<Customer> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_CUSTOMERS, customerMapper);
	}

	@Override
	public void remove(Long id) {
		jdbcTemplate.update(SQL_DELETE_CUSTOMER, id);
		log.debug("Customer with Id: {} was removed", id);
	}

	private Customer create(Customer customer) throws DAOException {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PARAMETER_FIRST_NAME, customer.getFirstName());
		parameters.put(PARAMETER_LAST_NAME, customer.getLastName());
		parameters.put(PARAMETER_DRIVER_LICENSE, customer.getDriverLicense());
		parameters.put(PARAMETER_LOCATION_ID, customer.getLocationId());
		try {
			Long createdId = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
			customer.setId(createdId);
			log.debug("create customer with id {}", createdId);

		} catch (DuplicateKeyException ex) {
			throw new DAOException(
					"This customer has an driver license which has already existed in the database!");
		} catch (DataIntegrityViolationException e) {
			throw new DAOException(
					"This customer has an location ID which does not exist in the database!");
		}
		return customer;
	}

	private Customer update(Customer customer) throws DAOException {
		try {
			jdbcTemplate.update(SQL_UPDATE_CUSTOMER, customer.getFirstName(), customer.getLastName(),
					customer.getDriverLicense(), customer.getLocationId(), customer.getId());
		} catch (
				DuplicateKeyException ex) {
			throw new DAOException(
					"This customer has an driver license which has already existed in the database!");
		} catch (
				DataIntegrityViolationException e) {
			throw new DAOException(
					"This customer has an location ID which does not exist in the database!");
		}
		log.debug("update customer with id {}", customer.getId());
		return customer;
	}
}
