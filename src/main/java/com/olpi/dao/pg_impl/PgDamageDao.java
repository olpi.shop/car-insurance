package com.olpi.dao.pg_impl;


import com.olpi.dao.DamageDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.DamageMapper;
import com.olpi.domain.Damage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Repository
public class PgDamageDao implements DamageDao {

	private static final String SQL_DAMAGE_TABLE = "damage";
	private static final String SQL_DAMAGE_ID = "damage_id";
	private static final String SQL_UPDATE_DAMAGE = "UPDATE  damage SET description = ?, damage_amount = ?, accident_id = ? WHERE damage_id = ?;";
	private static final String SQL_DELETE_DAMAGE = "DELETE FROM damage WHERE damage_id = ?;";
	private static final String SQL_SELECT_DAMAGE_BY_ID = "SELECT damage_id, description, damage_amount, accident_id FROM damage  WHERE damage_id = ?";
	private static final String SQL_SELECT_ALL_DAMAGES = "SELECT damage_id, description, damage_amount, accident_id FROM damage;";

	private static final String PARAMETER_DESCRIPTION = "description";
	private static final String PARAMETER_DAMAGE_AMOUNT = "damage_amount";
	private static final String PARAMETER_ACCIDENT_ID = "accident_id";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcInsert simpleJdbcInsert;
	@Autowired
	private DamageMapper damageMapper;

	@PostConstruct
	private void init() {
		simpleJdbcInsert = new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
				.withTableName(SQL_DAMAGE_TABLE).usingGeneratedKeyColumns(SQL_DAMAGE_ID);
		log.debug("SimpleJdbcInsert was created in PgDamageDao");
	}

	@Override
	public Damage createOrUpdate(Damage damage) throws DAOException {
		if (damage.getId() == null) {
			return create(damage);
		} else if (findById(damage.getId()).isPresent()) {
			return update(damage);
		} else {
			throw new DAOException(
					"This damage has an ID but does not exist in the database!");
		}
	}

	@Override
	public Optional<Damage> findById(Long id) {
		try {
			Damage damage = jdbcTemplate.queryForObject(SQL_SELECT_DAMAGE_BY_ID,
					new Object[]{id}, damageMapper);
			log.debug("Damage with ID: {} was found", id);
			return Optional.ofNullable(damage);
		} catch (EmptyResultDataAccessException e) {
			log.debug("There is not damage with ID: {}", id);
			return Optional.empty();
		}
	}

	@Override
	public List<Damage> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_DAMAGES, damageMapper);
	}

	@Override
	public void remove(Long id) {
		jdbcTemplate.update(SQL_DELETE_DAMAGE, id);
		log.debug("Damage with Id: {} was removed", id);
	}

	private Damage create(Damage damage) throws DAOException {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PARAMETER_DESCRIPTION, damage.getDescription());
		parameters.put(PARAMETER_DAMAGE_AMOUNT, damage.getDamageAmount());
		parameters.put(PARAMETER_ACCIDENT_ID, damage.getAccidentId());
		try {
			Long createdId = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
			damage.setId(createdId);
			log.debug("create damage with id {}", createdId);
		} catch (
				DataIntegrityViolationException e) {
			throw new DAOException(
					"This damage has an accident ID which does not exist in the database!");
		}
		return damage;
	}

	private Damage update(Damage damage) throws DAOException {
		try {
			jdbcTemplate.update(SQL_UPDATE_DAMAGE, damage.getDescription(), damage.getDamageAmount(), damage
					.getAccidentId(), damage.getId());
			log.debug("update damage with id {}", damage.getId());
		} catch (
				DataIntegrityViolationException e) {
			throw new DAOException(
					"This damage has an accident ID which does not exist in the database!");
		}
		return damage;
	}
}
