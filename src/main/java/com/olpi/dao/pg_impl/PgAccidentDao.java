package com.olpi.dao.pg_impl;

import com.olpi.dao.AccidentDao;
import com.olpi.dao.exception.DAOException;
import com.olpi.dao.mapper.AccidentMapper;
import com.olpi.domain.Accident;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;

@Slf4j
@Repository
public class PgAccidentDao implements AccidentDao {
	private static final String SQL_ACCIDENT_TABLE = "accident";
	private static final String SQL_ACCIDENT_ID = "accident_id";
	private static final String SQL_UPDATE_ACCIDENT = "UPDATE  accident SET report_id = ?, accident_date = ?, car_id = ?, location_id = ? WHERE accident_id = ?;";
	private static final String SQL_DELETE_ACCIDENT = "DELETE FROM accident WHERE accident_id = ?;";
	private static final String SQL_SELECT_ACCIDENT_BY_ID = "SELECT accident_id, report_id, accident_date, car_id, location_id FROM accident  WHERE accident_id = ?";
	private static final String SQL_SELECT_ALL_ACCIDENTS = "SELECT  accident_id, report_id, accident_date, car_id, location_id  FROM accident;";
	private static final String PARAMETER_REPORT_ID = "report_id";
	private static final String PARAMETER_ACCIDENT_DATE = "accident_date";
	private static final String PARAMETER_CAR_ID = "car_id";
	private static final String PARAMETER_LOCATION_ID = "location_id";

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private AccidentMapper accidentMapper;
	private SimpleJdbcInsert simpleJdbcInsert;

	@PostConstruct
	private void init() {
		simpleJdbcInsert = new SimpleJdbcInsert(Objects.requireNonNull(jdbcTemplate.getDataSource()))
				.withTableName(SQL_ACCIDENT_TABLE).usingGeneratedKeyColumns(SQL_ACCIDENT_ID);
		log.debug("SimpleJdbcInsert was created in PgAccidentDao");
	}

	@Override
	public Accident createOrUpdate(Accident accident) throws DAOException {
		if (accident.getId() == null) {
			return create(accident);
		} else if (findById(accident.getId()).isPresent()) {
			return update(accident);
		} else {
			throw new DAOException(
					"This accident has an ID but does not exist in the database!");
		}
	}

	@Override
	public Optional<Accident> findById(Long id) {
		try {
			Accident accident = jdbcTemplate.queryForObject(SQL_SELECT_ACCIDENT_BY_ID,
					new Object[]{id}, accidentMapper);
			log.debug("Accident with ID: {} was found", id);
			return Optional.ofNullable(accident);
		} catch (EmptyResultDataAccessException e) {
			log.debug("There is not accident with ID: {}", id);
			return Optional.empty();
		}
	}

	@Override
	public List<Accident> findAll() {
		return jdbcTemplate.query(SQL_SELECT_ALL_ACCIDENTS, accidentMapper);
	}

	@Override
	public void remove(Long id) {
		jdbcTemplate.update(SQL_DELETE_ACCIDENT, id);
		log.debug("Accident with Id: {} was removed", id);
	}

	private Accident create(Accident accident) throws DAOException {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put(PARAMETER_REPORT_ID, accident.getReportId());
		parameters.put(PARAMETER_ACCIDENT_DATE, accident.getAccidentDate());
		parameters.put(PARAMETER_CAR_ID, accident.getCarId());
		parameters.put(PARAMETER_LOCATION_ID, accident.getLocationId());
		try {
			Long createdId = simpleJdbcInsert.executeAndReturnKey(parameters).longValue();
			accident.setId(createdId);
			log.debug("create accident with id {}", createdId);
		} catch (DataIntegrityViolationException e) {
			throw new DAOException(
					"This accident has an location ID or car ID which does not exist in the database!");
		}
		return accident;
	}

	private Accident update(Accident accident) throws DAOException {
		try {
			jdbcTemplate.update(SQL_UPDATE_ACCIDENT, accident.getReportId(), accident.getAccidentDate(),
					accident.getCarId(), accident.getLocationId(), accident.getId());
		} catch (DataIntegrityViolationException e) {
			throw new DAOException(
					"This accident has an location ID or car ID which does not exist in the database!");
		}
		log.debug("update accident with id {}", accident.getId());
		return accident;
	}

}
