package com.olpi.dao;

import com.olpi.dao.exception.DAOException;
import com.olpi.domain.Accident;
import com.olpi.domain.Car;

import java.util.List;
import java.util.Optional;

public interface CarDao extends EntityDao<Long, Car>{

    Car createOrUpdate(Car car) throws DAOException;

    Optional<Car> findById(Long id);

    List<Car> findAll();

    void remove(Long id);
}
